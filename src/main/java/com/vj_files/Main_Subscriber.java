package com.vj_files;

import org.eclipse.paho.client.mqttv3.MqttException;

import javax.swing.*;
import java.io.File;

public class Main_Subscriber {

    public static void main(String[] args) {


        /*
        GUI_MQTTsubscriber guiSub = GUI_MQTTsubscriber.createFromJSON(new File("/Users/B8/Documents/progJava/vj_mqtt_1.3.2/final_gui.json"));
        //guiSub.listenForMessage();

        guiSub.start();

        // swing invoke later

         */

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                GUI_MQTTsubscriber guiSub = GUI_MQTTsubscriber.createFromJSON(new File("/Users/B8/Documents/progJava/vj_mqtt_1.3.2/final_gui.json"));
                guiSub.start();
            }
        });

    }

}
