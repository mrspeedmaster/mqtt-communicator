package com.vj_files;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttException;

public class MQTTSubscriber {
    private String serverURL;
    private String topic;
    private MqttClient client;

    private vjMqttCallBack mqttcallback1;

    @JsonCreator
    public MQTTSubscriber(@JsonProperty("serverURL") String serverURL, @JsonProperty("topic") String topic) throws MqttException {
        this.serverURL = serverURL;
        this.topic = topic;
        try {
            this.createMQTTClient();
        } catch (MqttException e) {
            e.getMessage();
        }
    }

    private void createMQTTClient() throws MqttException {
        client = new MqttClient(serverURL, MqttClient.generateClientId());
        //client.setCallback(new vjMqttCallBack());

        //mqttcallback1 = new vjMqttCallBack();

        //client.setCallback(mqttcallback1);
        //client.setCallback(new GUI_MQTTsubscriber());
    }

    public void setCallback(MqttCallback cb) {
        client.setCallback(cb);
    }

    public void connect() throws MqttException {
        client.connect();
        client.subscribe(topic);
    }

    public void disconnect() throws MqttException {
        client.disconnect();
    }

    public String getMessage() {
        return mqttcallback1.getMessage();
    }

}
