package com.vj_files;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Random;

@JsonPropertyOrder({"factor", "unit", "lower_limit", "upper_limit"}) // ovo dodam ako zelim odrediti redosljed membera
public class Senzor {
    private int factor;
    private String unit;
    private int lower_limit;
    private int upper_limit;

    @JsonCreator
    public Senzor(@JsonProperty("lower_limit") int lower_limit, @JsonProperty("upper_limit") int upper_limit, @JsonProperty("unit") String unit, @JsonProperty("factor") int factor) {
        this.factor = factor;
        this.unit = unit;
        this.lower_limit = lower_limit;
        this.upper_limit = upper_limit;
    }

    public void setFactor(int factor) {
        this.factor = factor;
    }

    public int getFactor() {
        return factor;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getUnit() {
        return unit;
    }

    public void setLower_limit(int lower_limit) {
        this.lower_limit = lower_limit;
    }

    public int getLower_limit() {
        return lower_limit;
    }

    public void setUpper_limit(int upper_limit) {
        this.upper_limit = upper_limit;
    }

    public int getUpper_limit() {
        return upper_limit;
    }

    public float generateRandomValue() {
        Random rand = new Random();
        // upper_limit nesmije biti manji od lower limit jer ce dati exception
        return (float)(rand.nextInt((upper_limit - lower_limit) + 1) + lower_limit) / factor;
    }
}
