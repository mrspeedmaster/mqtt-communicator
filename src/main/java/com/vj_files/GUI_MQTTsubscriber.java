package com.vj_files;

import javax.swing.*;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;

public class GUI_MQTTsubscriber implements MqttCallback {
    private JFrame frame;
    private JPanel panel;
    private JLabel label;
    private MQTTSubscriber mqttSub;

    private JTextArea msg;

    @JsonCreator
    public GUI_MQTTsubscriber(@JsonProperty("title") String title, @JsonProperty("width") int width, @JsonProperty("height") int height, @JsonProperty("labela") String labela) {

        frame = new JFrame();

        frame.setTitle(title); // ovde json
        frame.setSize(width, height); // ovde json
        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        frame.addWindowListener(new WindowAdapter() { // ovdje sam disconnectao mqtt clienta kada stisnem X dugme
            @Override
            public void windowClosing(WindowEvent e) {
                try {
                    mqttSub.disconnect();
                } catch (MqttException ex) {
                    ex.printStackTrace();
                }
                super.windowClosing(e);
            }
        });

        label = new JLabel();
        label.setText(labela); // ovde json

        msg = new JTextArea();
        msg.setText("");
        msg.setEditable(false);

        panel = new JPanel();
        panel.add(label);
        panel.add(msg);

        frame.add(panel);

        frame.setVisible(true); // setVisible pozovem zadnje, jer zelim renderat prozor kad dodam sve elemente, ako
        // ... pozovem ranije onda mi se nece prikazati elementi(labele...) dok ne resizam window...

    }

    public static GUI_MQTTsubscriber createFromJSON(File f) {
        try {
            ObjectMapper mapper = new ObjectMapper().enable(SerializationFeature.INDENT_OUTPUT);

            GUI_MQTTsubscriber guisub;

            guisub = mapper.readValue(f, GUI_MQTTsubscriber.class);

            guisub.start();

            return guisub;

        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("error in gui subsctiber, methon createfromJSON, returned null");
        return null;
    }

    public void setMqttSub(MQTTSubscriber mqttSub) {
        this.mqttSub = mqttSub;
    }

    public MQTTSubscriber getMqttSub() {
        return mqttSub;
    }

    public void start() {
        try {

            mqttSub.setCallback(this); /// ovo

            mqttSub.connect();

        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void updateLabel() {
        msg.setText(mqttSub.getMessage());
    }

    public void listenForMessage() {
        while (true) {
            try {
                Thread.sleep(5000);
                updateLabel();
            } catch (Exception e) {
                e.getMessage();
            }
        }
    }

    public void connectionLost(Throwable throwable) {
        System.out.println("Connection lost");
    }

    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
        System.out.println("Message received: " + new String(mqttMessage.getPayload()));
        msg.setText(new String(mqttMessage.getPayload()));
    }

    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }

    /*
    public static void main(String[] args) {
        GUI_MQTTsubscriber guisub1 = GUI_MQTTsubscriber.createFromJSON(new File("/Users/B8/Documents/progJava/vj_mqtt_1.3.1/final_gui.json"));
        guisub1.listenForMessage();
    }
     */
}
