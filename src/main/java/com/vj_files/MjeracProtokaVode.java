package com.vj_files;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttException;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

// jackson, setters i getters su potrebni da bi de/serijalizacija radila...

@JsonPropertyOrder({"serverURL", "topic", "senzorList"}) // ovo dodam ako zelim odrediti redosljed membera
public class MjeracProtokaVode {
    private String serverURL;
    private String topic;
    private List<Senzor> senzorList = new ArrayList<Senzor>();

    @JsonCreator
    public MjeracProtokaVode(@JsonProperty("serverURL") String url, @JsonProperty("topic") String topic) {
        this.serverURL = url;
        this.topic = topic;
    }

    public static MjeracProtokaVode createFromJSON(File f) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.enable(SerializationFeature.INDENT_OUTPUT);

            MjeracProtokaVode mjerac;
            mjerac = mapper.readValue(f, MjeracProtokaVode.class);

            //System.out.println("parse static method mapper");
            //String str2 = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(tmp);
            //System.out.println(str2);

            return mjerac;
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("error in parse static method, returned null");
        return null;
    }

    public void setServerURL(String serverURL) {
        this.serverURL = serverURL;
    }

    public String getServerURL() {
        return serverURL;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getTopic() {
        return topic;
    }

    public void setSenzorList(List<Senzor> senzorList) {
        this.senzorList = senzorList;
    }

    public List<Senzor> getSenzorList() {
        return senzorList;
    }

    public void publishValues() throws MqttException, InterruptedException {

        while (true) {
            Thread.sleep(5000);

            // formiranje String poruke
            String strMsg = "Vrijednosti:\n";
            int i = 1;
            for (Senzor s : senzorList) {
                strMsg = strMsg.concat("Senzor " + i + ": " + s.generateRandomValue() + " " + s.getUnit() + '\n');
                i++;
            }

            System.out.println(strMsg);

            // spajanje na server i slanje poruke
            MqttClient client = new MqttClient(serverURL, MqttClient.generateClientId());
            client.connect();
            MqttMessage message = new MqttMessage();
            message.setPayload(strMsg.getBytes());
            client.publish(topic, message);
            client.disconnect();

        }
    }

    public void printPrettyJSON() {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            System.out.println(mapper.writeValueAsString(this));
            return;
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("error printing pretty JSON");
        return;
    }

}
