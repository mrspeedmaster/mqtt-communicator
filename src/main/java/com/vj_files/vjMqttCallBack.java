package com.vj_files;

import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttMessage;

public class vjMqttCallBack implements MqttCallback {
    private String message = "poruka jos nije stigla";

    public String getMessage() {
        return message;
    }

    public void connectionLost(Throwable throwable) {
        System.out.println("Connection lost");
    }

    public void messageArrived(String s, MqttMessage mqttMessage) throws Exception {
        System.out.println("Message received: " + new String(mqttMessage.getPayload()));
        message = new String(mqttMessage.getPayload());
    }

    public void deliveryComplete(IMqttDeliveryToken iMqttDeliveryToken) {

    }

}
